import request from '@/utils/request'

export function customerList(data) {
  return request({
    url: '/customer/list',
    method: 'get',
    params: data
  })
}
export function createCustomer(data) {
  return request({
    url: '/customer/create',
    method: 'POST',
    data: data
  })
}

export function tradeAccountList(data) {
  return request({
    url: '/customer/list',
    method: 'get',
    params: data
  })
}

export function addBankCard(data) {
  return request({
    url: '/customer/bank_card/create_batch',
    method: 'POST',
    data: data
  })
}

export function bankCardList(data) {
  return request({
    url: '/customer/bank_card/list',
    method: 'get',
    params: data
  })
}
export function fundTradeAccount(data) {
  return request({
    url: '/customer/fund_trade_account/list',
    method: 'get',
    params: data
  })
}
