import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/system/admin/list',
    method: 'get',
    params: data
  })
}

export function createAdmin(data) {
  return request({
    url: '/system/admin/create',
    method: 'post',
    data: data
  })
}
export function updateAdmin(data) {
  return request({
    url: '/system/admin/update',
    method: 'post',
    data: data
  })
}

export function deleteAdmin(data) {
  return request({
    url: '/system/admin/delete',
    method: 'post',
    data: data
  })
}
