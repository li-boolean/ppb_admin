import request from '@/utils/request'

export function getRoleList(data) {
  return request({
    url: '/system/role/list',
    method: 'get',
    params: data
  })
}
export function createRole(data) {
  return request({
    url: '/system/role/create',
    method: 'post',
    data: data
  })
}
export function updateRole(data) {
  return request({
    url: '/system/role/update',
    method: 'post',
    data: data
  })
}
export function deleteRole(data) {
  return request({
    url: '/system/role/delete',
    method: 'post',
    data: data
  })
}

export function getRoleAll(data) {
  return request({
    url: '/system/role/all',
    method: 'get',
    params: data
  })
}
