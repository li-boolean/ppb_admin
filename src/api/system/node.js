import request from '@/utils/request'

export function getNodes(data) {
  return request({
    url: '/system/node/list',
    method: 'get',
    params: data
  })
}
export function createNode(data) {
  return request({
    url: '/system/node/create',
    method: 'post',
    data: data
  })
}
export function updateNode(data) {
  return request({
    url: '/system/node/update',
    method: 'post',
    data: data
  })
}
export function deleteNode(data) {
  return request({
    url: '/system/node/delete',
    method: 'post',
    data: data
  })
}
