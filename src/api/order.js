import request from '@/utils/request'

export function orderList(data) {
  return request({
    url: '/order/order/list',
    method: 'get',
    params: data
  })
}
export function orderDetail(data) {
  return request({
    url: '/order/order/detail',
    method: 'get',
    params: data
  })
}

export function createSubscribeOrder(data) {
  return request({
    url: '/order/subscribe/create',
    method: 'post',
    data: data
  })
}
export function createRedeemOrder(data) {
  return request({
    url: '/order/redeem/create',
    method: 'post',
    data: data
  })
}
export function payOrder(data) {
  return request({
    url: '/order/subscribe/pay',
    method: 'post',
    data: data
  })
}
export function confirmTradeFinished(data) {
  return request({
    url: '/order/order/confirm_order_trade_finished',
    method: 'post',
    data: data
  })
}

export function cancelOrder(data) {
  return request({
    url: '/order/order/cancel',
    method: 'post',
    data: data
  })
}
export function addBill(data) {
  return request({
    url: '/order/order/add_bill',
    method: 'post',
    data: data
  })
}

export function pubBankBillList(data) {
  return request({
    url: '/order/pub_bank_bill/list',
    method: 'get',
    params: data
  })
}

export function orderLogList(data) {
  return request({
    url: '/order/log/list',
    method: 'get',
    params: data
  })
}

export function orderBillList(data) {
  return request({
    url: '/order/bill/list',
    method: 'get',
    params: data
  })
}

export function checkOrder(data) {
  return request({
    url: '/order/order/check_finished',
    method: 'post',
    data: data
  })
}

export function redeemReApply(data) {
  return request({
    url: '/order/redeem/reapply',
    method: 'post',
    data: data
  })
}

export function subscribeReApply(data) {
  return request({
    url: '/order/subscribe/reapply',
    method: 'post',
    data: data
  })
}
